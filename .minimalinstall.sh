#!/bin/bash

# Dotfile Dependencies
yay -S --needed bspwm sxhkd wpgtk-git feh polybar dunst rofi python-dbus python-gobject pacman-contrib siji-git dina-font git gtk-engine-murrine xsettingsd pfetch htop xorg lightdm-gtk-greeter pulseaudio pulseaudio-alsa

# Optional Dependencies
yay -S --needed termite scrot emacs firefox pcmanfm-gtk3 lxappearance-gtk3

systemctl enable --user pulseaudio

systemctl enable lightdm

wpg-install.sh -gi
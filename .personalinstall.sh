#!/bin/bash

# Dotfile Dependencies
yay -S --needed bspwm sxhkd wpgtk-git feh polybar dunst rofi python-dbus python-gobject pacman-contrib siji-git dina-font git gtk-engine-murrine xsettingsd pfetch htop xorg lightdm-gtk-greeter pulseaudio pulseaudio-alsa

# Optional Dependencies
yay -S --needed termite scrot emacs firefox pcmanfm-gtk3 lxappearance-gtk3

# Personal Dependencies
yay -S --needed firefox chromium chromium-widevine torbrowser-launcher steam steamguard-cli-git discord pfetch-git freetube youtube-dl unityhub libreoffice-fresh lyx kdenlive lollypop audacity musescore vlc playonlinux gimp krita emacs texlive-most xf86-input-wacom gnupg pass qtpass noto-fonts noto-fonts-extra noto-fonts-emoji noto-fonts-cjk

systemctl enable --user pulseaudio

systemctl enable lightdm

wpg-install.sh -gi